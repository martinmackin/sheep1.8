﻿import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/index';
import { LoginComponent } from './login/index';
import { RegisterComponent } from './register/index';
import { AuthGuard } from './_guards/index';

import { EagerComponent } from './temp/eager.component';
import { FlckmngComponent } from './flckmng/flckmng.component';

// import { EwesComponent } from './flck-mng/index';
// import { EwesListComponent } from './flck-mng/index';
// import { LambsComponent } from './flck-mng/index';
// import { RamsComponent } from './flck-mng/index';

// path '' diverts to homeCompent template
const routes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'eager', component: EagerComponent },
    { path: 'flckmng', component: FlckmngComponent, canActivate: [AuthGuard] },
    { path: 'rprts', loadChildren: 'app/__rprts/rprts.module#RprtsModule', canActivate: [AuthGuard] },

    // otherwise redirect to home
    { path: '**', redirectTo: 'HomeComponent' }

];



// export const routing = RouterModule.forRoot(appRoutes);
export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
