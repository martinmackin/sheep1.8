import { Component, OnInit, NgZone } from '@angular/core';
import { MatSidenav } from '@angular/material';
// import { Router } from '@angular/router';
// import { MatSidenav } from '@angular/material';

const SMALL_WIDTH_BREAKPOINT = 720;

@Component({
  selector: 'app-layout-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  private mediaMatcher: MediaQueryList =
  matchMedia(`(max-width: ${SMALL_WIDTH_BREAKPOINT}px)`);

  constructor(zone: NgZone) {
    this.mediaMatcher.addListener(mql =>
      zone.run(() => this.mediaMatcher = mql));
  }
    ngOnInit() {
    }

    isScreenSmall(): boolean {
      return this.mediaMatcher.matches;
    }
  }
  