import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RprtsComponent } from './rprts.component';

const routes: Routes = [
  { path: '', component: RprtsComponent }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
