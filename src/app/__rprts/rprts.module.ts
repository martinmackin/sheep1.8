import { NgModule } from '@angular/core';

import { RprtsComponent } from './rprts.component';
import { routing } from './rprts.routing';

@NgModule({
  imports: [routing],
  declarations: [RprtsComponent]
})
export class RprtsModule {}
