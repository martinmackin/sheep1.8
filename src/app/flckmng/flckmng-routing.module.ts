import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from '../home/home.component';
import { LoginComponent } from '../login/login.component';
import { RegisterComponent } from '../register/register.component';
import { FlckmngComponent } from '../flckmng/flckmng.component';
import { HeaderComponent } from '../_shared/layout/header.component';
import { FooterComponent } from '../_shared/layout/footer.component';
import { EwesListComponent } from '../flckmng/component/ewes-list.component';

import { AuthGuard } from '../_guards/index';


const routes: Routes = [
  { path: '', component: FlckmngComponent,
    children: [
      { path: '', component: EwesListComponent }
    ] },
    { path: '**', redirectTo: '' }
];


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class FlckmngRoutingModule { }
