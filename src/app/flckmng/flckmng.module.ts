import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { MaterialModule } from '../_shared/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


// starting to import libraries material, flexlayout


import { FlckmngRoutingModule } from './flckmng-routing.module';
import { FlckmngComponent } from './flckmng.component';
import { EwesListComponent } from './component/ewes-list.component';

@NgModule({
  imports: [
    CommonModule,
    FlckmngRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [FlckmngComponent, EwesListComponent ]
})
export class FlckmngModule { }
