import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EwesListComponent } from './ewes-list.component';

describe('EwesListComponent', () => {
  let component: EwesListComponent;
  let fixture: ComponentFixture<EwesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EwesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EwesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
